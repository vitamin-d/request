<?php
return [
    'ctrl' => [
        'title' => 'Anfrage',
        'label' => 'title',
        'label_userFunc' => \VITD\Request\Tca\Helper::class . '->label',
        'default_sortby' => 'date DESC',
        'delete' => 'deleted',
        'enablecolumns' => [],
        // table as whole must be readable to allow deleting entries via backend.
        // instead, individual fields are made readonly
        'readOnly' => false,
        'searchFields' => 'type, title, content, ',
        'iconfile' => 'EXT:request/Resources/Public/Icons/tx_request_request.svg',
    ],
    'interface' => ['showRecordFieldList' => 'type, title, date, content'],
    'types' => [0 => ['showitem' => 'type, title, date, content']],
    'columns' => [
        'type' => [
            'exclude' => true,
            'label' => 'Typ',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'max' => 64,
                'readOnly' => true,
            ],
        ],
        'date' => [
            'exclude' => true,
            'label' => 'Datum und Uhrzeit',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'dbType' => 'datetime',
                'eval' => 'datetime',
                'readOnly' => true,
            ],
        ],
        'title' => [
            'exclude' => true,
            'label' => 'Titel',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'max' => 255,
                'readOnly' => true,
            ],
        ],
        'content' => [
            'exclude' => true,
            'label' => 'Inhalt',
            'config' => [
                'type' => 'text',
                'cols' => 80,
                'rows' => 20,
                'wrap' => 'off',
                'readOnly' => true,
            ],
        ],
    ],
];
