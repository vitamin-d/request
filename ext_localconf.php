<?php
defined('TYPO3_MODE') or die();

(function ($packageKey) {
    $GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include'][$packageKey] = \VITD\Request\Handler::class . '::handle';
})('request');
