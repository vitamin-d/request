<?php

namespace VITD\Request;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


/**
 * Finishing interface
 */
interface Finishing
{
    /**
     * Invoke this finisher
     *
     * @param string $id Form identification
     * @param ServerRequestInterface $request Request
     * @param ResponseInterface $response Current state of the response
     * @param array $arguments List of arguments for this invocation
     *
     * @return ResponseInterface Response
     */
    public function invoke(
        string $id,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $arguments = []
    ): ResponseInterface;
}
