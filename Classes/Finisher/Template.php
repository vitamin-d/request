<?php

namespace VITD\Request\Finisher;

use GuzzleHttp\Psr7\BufferStream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Fluid\View\StandaloneView;
use VITD\Request\Finishing;

/**
 * Template finisher
 */
class Template implements Finishing
{

    /**
     * Invoke this finisher
     *
     * @param string $id Form identification
     * @param ServerRequestInterface $request Request
     * @param ResponseInterface $response Current state of the response
     * @param array $arguments List of arguments for this invocation
     *
     * @return ResponseInterface Response
     *
     * @throws \BadMethodCallException 1516864693 If the template name is not defined
     * @throws \BadMethodCallException 1516864892 If the template paths are not defined
     * @throws \InvalidArgumentException 1516865357 If a template path is invalid
     * @throws \BadMethodCallException 1516865523 If the layout paths are not defined
     * @throws \InvalidArgumentException 1516865524 If a layout path is invalid
     * @throws \BadMethodCallException 1516865525 If the partial paths are not defined
     * @throws \InvalidArgumentException 1516865526 If a partial path is invalid
     */
    public function invoke(
        string $id,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $arguments = []
    ): ResponseInterface
    {
        $this->processArguments($arguments);

        $renderer = GeneralUtility::makeInstance(ObjectManager::class)->get(StandaloneView::class);
        $renderer->setTemplateRootPaths($arguments['templateRootPaths']);
        $renderer->setLayoutRootPaths($arguments['layoutRootPaths']);
        $renderer->setPartialRootPaths($arguments['partialRootPaths']);
        $renderer->setTemplate($arguments['templateName']);
        $renderer->setFormat('html');
        $renderer->assignMultiple([
            'fields' => $arguments['fields'],
            'request' => $request,
            'response' => $response,
        ]);

        return $response
            ->withStatus(200)
            ->withBody(
                (function() use ($renderer) {
                    $stream = GeneralUtility::makeInstance(BufferStream::class);
                    $stream->write($renderer->render());
                    return $stream;
                })()
            );
    }



    // ---------------------- internal helper methods -----------------------
    /**
     * Process and validate arguments (in place)
     *
     * @param array $arguments List of arguments
     *
     * @return void
     *
     * @throws \BadMethodCallException 1516864693 If the template name is not defined
     * @throws \BadMethodCallException 1516864892 If the template paths are not defined
     * @throws \InvalidArgumentException 1516865357 If a template path is invalid
     * @throws \BadMethodCallException 1516865523 If the layout paths are not defined
     * @throws \InvalidArgumentException 1516865524 If a layout path is invalid
     * @throws \BadMethodCallException 1516865525 If the partial paths are not defined
     * @throws \InvalidArgumentException 1516865526 If a partial path is invalid
     */
    protected function processArguments(array &$arguments): void
    {
        if (
            !array_key_exists('templateName', $arguments)
            || !\is_string($arguments['templateName'])
            || '' === $arguments['templateName']
        ) {
            throw new \BadMethodCallException('Template name not defined', 1516864693);
        }

        if (
            !array_key_exists('templateRootPaths', $arguments)
            || !\is_array($arguments['templateRootPaths'])
            || empty($arguments['templateRootPaths'])
        ) {
            throw new \BadMethodCallException('Template paths not defined', 1516864892);
        }
        array_walk($arguments['templateRootPaths'], function(&$path, $i) {
            $result = GeneralUtility::getFileAbsFileName($path);
            if ($result === '') {
                throw new \InvalidArgumentException('Invalid template path "' . $path . '"', 1516865357);
            }
            $path = $result;
        });

        if (
            !array_key_exists('layoutRootPaths', $arguments)
            || !\is_array($arguments['layoutRootPaths'])
            || empty($arguments['layoutRootPaths'])
        ) {
            throw new \BadMethodCallException('Layout paths not defined', 1516865523);
        }
        array_walk($arguments['layoutRootPaths'], function(&$path, $i) {
            $result = GeneralUtility::getFileAbsFileName($path);
            if ($result === '') {
                throw new \InvalidArgumentException('Invalid layout path "' . $path . '"', 1516865524);
            }
            $path = $result;
        });

        if (
            !array_key_exists('partialRootPaths', $arguments)
            || !\is_array($arguments['partialRootPaths'])
            || empty($arguments['partialRootPaths'])
        ) {
            throw new \BadMethodCallException('Partial paths not defined', 1516865525);
        }
        array_walk($arguments['partialRootPaths'], function(&$path, $i) {
            $result = GeneralUtility::getFileAbsFileName($path);
            if ($result === '') {
                throw new \InvalidArgumentException('Invalid partial path "' . $path . '"', 1516865526);
            }
            $path = $result;
        });
    }
}
