<?php

namespace VITD\Request\Finisher;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use VITD\Request\Finishing;

/**
 * Database finisher
 */
class Database implements Finishing
{

    /**
     * Invoke this finisher
     *
     * @param string $id Form identification
     * @param ServerRequestInterface $request Request
     * @param ResponseInterface $response Current state of the response
     * @param array $arguments List of arguments for this invocation
     *
     * @return ResponseInterface Response
     *
     * @throws \BadMethodCallException 1516815504 If the form storage pid is not defined or invalid
     * @throws \BadMethodCallException 1516871565 If the title field is not defined
     * @throws \InvalidArgumentException 1516871694 If the title field is invalid
     * @throws \InvalidArgumentException 1516871777 If the specified title field does not exist
     */
    public function invoke(
        string $id,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $arguments = []
    ): ResponseInterface
    {
        if (!array_key_exists('pid', $arguments) || !\is_int($arguments['pid'])) {
            throw new \BadMethodCallException('Form storage pid not defined or invalid', 1516815504);
        }
        if (!array_key_exists('titlefield', $arguments)) {
            throw new \BadMethodCallException('Title field not defined', 1516871565);
        }
        if (!\is_string($arguments['titlefield']) || '' === $arguments['titlefield']) {
            throw new \InvalidArgumentException('Title field is invalid', 1516871694);
        }
        if (!array_key_exists($arguments['titlefield'], $arguments['fields'])) {
            throw new \InvalidArgumentException('Specified a field as title field which does not exist', 1516871777);
        }

        $content = '';
        foreach($arguments['fields'] as $name => $value) {
            $content .= wordwrap($name . ': ' . $value, 72, LF . '    ') . LF;
        }

        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_request_request')
            ->insert('tx_request_request')
            ->values([
                'pid' => $arguments['pid'],
                'type' => $id,
                'title' => mb_strcut($arguments['fields'][$arguments['titlefield']], 0, 255),
                'content' => $content,
            ])
            ->execute();

        return $response;
    }
}
