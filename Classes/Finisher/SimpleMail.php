<?php

namespace VITD\Request\Finisher;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Ramsey\Uuid\UuidFactory;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MailUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use VITD\Request\Finishing;

/**
 * Mail finisher
 */
class SimpleMail implements Finishing
{

    /**
     * Invoke this finisher
     *
     * @param string $id Form identification
     * @param ServerRequestInterface $request Request
     * @param ResponseInterface $response Current state of the response
     * @param array $arguments List of arguments for this invocation
     *
     * @return ResponseInterface Response
     *
     * @throws \BadMethodCallException 1516813789 If the form mail recipient(s) is not defined
     * @throws \InvalidArgumentException 1516813812 If the form mail recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813814 If the form mail recipient(s) is invalid
     * @throws \BadMethodCallException 1516813815 If the form mail subject is missing or invalid
     * @throws \InvalidArgumentException 1516813866 If the form mail additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813867 If the form mail additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813939 If the form mail hidden additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813940 If the form mail hidden additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813980 If the form mail reply address is invalid
     * @throws \InvalidArgumentException 1516813981 If the form mail reply address is invalid
     * @throws \RuntimeException 1517305937 If an upload error happens
     */
    public function invoke(
        string $id,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $arguments = []
    ): ResponseInterface
    {
        $this->processArguments($arguments);

        $content = 'Formular: ' . $id . LF;
        foreach($arguments['fields'] as $name => $value) {
            $content .= wordwrap($name . ': ' . $value, 72, LF . '    ') . LF;
        }

        if (array_key_exists('storeFilesInFolder', $arguments)) {
            $files = $this->copyFiles($arguments['storeFilesInFolder']);
            if ($files !== []) {
                $content .= LF . LF . 'Dateien: ' . LF;
            }
            foreach ($files as $file) {
                /** @var File $file */
                $content .=
                    $file->getName() . ':' . LF
                    . '  '
                    . $request->getServerParams()['REQUEST_SCHEME'] . '://' . $request->getServerParams()['SERVER_NAME']
                    . dirname($request->getServerParams()['SCRIPT_NAME']) . '/'
                    .  $file->getPublicUrl() . LF . LF;
            }
        }

        $message = GeneralUtility::makeInstance(MailMessage::class)
            ->setTo($arguments['to'])
            ->setSubject($arguments['subject'])
            ->setBody($content, 'text/plain', 'utf8');

        if (null !== MailUtility::getSystemFrom()) {
            $message->setFrom(MailUtility::getSystemFrom());
        }
        if (array_key_exists('cc', $arguments)) {
            $message->setCc($arguments['cc']);
        }
        if (array_key_exists('bcc', $arguments)) {
            $message->setBcc($arguments['bcc']);
        }
        if (array_key_exists('replyto', $arguments)) {
            $message->setReplyTo($arguments['replyto']);
        }
        $message->send();

        return $response;
    }




    // ---------------------- internal helper methods -----------------------
    /**
     * Copy any uploaded files into a specified FAL folder
     *
     * @param string $folderObjectCombinedIdentifier Combined FAL folder identifier to move the files into
     *
     * @return array List of File objects that have been uploaded and moved to a subfolder in the specified folder
     *
     * @throws \RuntimeException 1517305937 If an upload error happens
     */
    protected function copyFiles($folderObjectCombinedIdentifier): array
    {
        $files = [];
        if (\is_array($_FILES)) {

            $folder = GeneralUtility
                ::makeInstance(ObjectManager::class)
                ->get(ResourceFactory::class)
                ->getFolderObjectFromCombinedIdentifier($folderObjectCombinedIdentifier);
            $folder = $folder->hasFolder(date('Y-m-d'))
                ? $folder->getSubfolder(date('Y-m-d'))
                : $folder->createFolder(date('Y-m-d'));
            $folder = $folder->createFolder((new UuidFactory())->uuid4());

            foreach(array_keys($_FILES) as $i) {
                if (\is_array($_FILES[$i]['tmp_name'])) {
                    foreach(array_keys($_FILES[$i]['tmp_name']) as $j) {
                        if ($_FILES[$i]['error'][$j] !== UPLOAD_ERR_OK) {
                            throw new \RuntimeException('Upload error', 1517305937); // TODO: Improve error message/handling
                        }
                        $files[] = $folder->addUploadedFile(
                            [
                                'tmp_name' => $_FILES[$i]['tmp_name'][$j],
                                'name' => $_FILES[$i]['name'][$j],
                                'type' => $_FILES[$i]['type'][$j],
                                'size' => $_FILES[$i]['size'][$j],
                                'error' => $_FILES[$i]['error'][$j],
                            ],
                            DuplicationBehavior::RENAME
                        );
                    }
                } elseif (\is_string($_FILES[$i]['tmp_name'])) {
                    if ($_FILES[$i]['error'] !== UPLOAD_ERR_OK) {
                        throw new \RuntimeException('Upload error', 1517305937); // TODO: Improve error message/handling
                    }
                    $files[] = $folder->addUploadedFile($_FILES[$i], DuplicationBehavior::RENAME);
                }
            }
        }
        return $files;
    }


    /**
     * Process and validate arguments (in place)
     *
     * @param array $arguments List of arguments
     *
     * @return void
     *
     * @throws \BadMethodCallException 1516813789 If the form mail recipient(s) is not defined
     * @throws \InvalidArgumentException 1516813812 If the form mail recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813814 If the form mail recipient(s) is invalid
     * @throws \BadMethodCallException 1516813815 If the form mail subject is missing or invalid
     * @throws \InvalidArgumentException 1516813866 If the form mail additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813867 If the form mail additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813939 If the form mail hidden additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813940 If the form mail hidden additional recipient(s) is invalid
     * @throws \InvalidArgumentException 1516813980 If the form mail reply address is invalid
     * @throws \InvalidArgumentException 1516813981 If the form mail reply address is invalid
     *
     * @todo: verify "storeFilesInFolder" optional argument
     */
    protected function processArguments(array &$arguments): void
    {
        if (!array_key_exists('to', $arguments)) {
            throw new \BadMethodCallException('Form mail recipient(s) not defined', 1516813789);
        }
        if (\is_string($arguments['to'])) {
            $arguments['to'] = [$arguments['to'] => null];
        }
        if (!\is_array($arguments['to'])) {
            throw new \InvalidArgumentException('Form mail recipient(s) invalid', 1516813812);
        }
        foreach($arguments['to'] as $address => $name) {
            if(!filter_var($address, FILTER_VALIDATE_EMAIL)) {
                throw new \InvalidArgumentException('Form mail recipient(s) invalid', 1516813814);
            }
        }

        if (!array_key_exists('subject', $arguments) || !\is_string($arguments['subject'])) {
            throw new \BadMethodCallException('Form mail subject missing or invalid', 1516813815);
        }


        // ----------------------- optional arguments -----------------------
        if (array_key_exists('cc', $arguments)) {
            if (\is_string($arguments['cc'])) {
                $arguments['cc'] = [$arguments['cc']];
            }
            if (!\is_array($arguments['cc'])) {
                throw new \InvalidArgumentException('Form mail additional recipient(s) invalid', 1516813866);
            }
            foreach($arguments['cc'] as $address) {
                if(!filter_var($address, FILTER_VALIDATE_EMAIL)) {
                    throw new \InvalidArgumentException('Form mail additional recipient(s) invalid', 1516813867);
                }
            }
        }

        if (array_key_exists('bcc', $arguments)) {
            if (\is_string($arguments['bcc'])) {
                $arguments['bcc'] = [$arguments['bcc']];
            }
            if (!\is_array($arguments['bcc'])) {
                throw new \InvalidArgumentException('Form mail hidden additional recipient(s) invalid', 1516813939);
            }
            foreach($arguments['bcc'] as $address) {
                if(!filter_var($address, FILTER_VALIDATE_EMAIL)) {
                    throw new \InvalidArgumentException('Form mail hidden additional recipient(s) invalid', 1516813940);
                }
            }
        }

        if (array_key_exists('replyto', $arguments)) {
            if (\is_string($arguments['replyto'])) {
                $arguments['replyto'] = [$arguments['replyto']];
            }
            if (!\is_array($arguments['replyto'])) {
                throw new \InvalidArgumentException('Form mail reply address invalid', 1516813980);
            }
            foreach($arguments['replyto'] as $address) {
                if(!filter_var($address, FILTER_VALIDATE_EMAIL)) {
                    throw new \InvalidArgumentException('Form mail reply address invalid', 1516813981);
                }
            }
        }
    }
}
