<?php

namespace VITD\Request;

use GuzzleHttp\Psr7\BufferStream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface;


/**
 * Request handler
 */
class Handler
{

    /**
     * Handle a request
     *
     * @param ServerRequestInterface $request Request
     * @param ResponseInterface $response Response
     *
     * @return ResponseInterface Response
     */
    public function handle(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // check request type
        if ($request->getMethod() !== 'POST') {
            return $response
                ->withStatus(404)
                ->withBody(
                    (function(){
                        $stream = GeneralUtility::makeInstance(BufferStream::class);
                        $stream->write('Form must be submitted using POST method.');
                        return $stream;
                    })()
                );
        }

        // check if form is properly defined
        if (
            !array_key_exists('form', $request->getQueryParams())
            || !\is_string($request->getQueryParams()['form'])
            || $request->getQueryParams()['form'] === ''
        ) {
            return $response
                ->withStatus(404)
                ->withBody(
                    (function(){
                        $stream = GeneralUtility::makeInstance(BufferStream::class);
                        $stream->write('You did not specify which form was submitted: Parameter "form" is missing, empty or not a string.');
                        return $stream;
                    })()
                );
        }
        try {
            /** @noinspection PhpInternalEntityUsedInspection */
            $definition = Manager::getFormDefinition($request->getQueryParams()['form']);
        } catch (\RuntimeException $e) {
            return $response
                ->withStatus(404)
                ->withBody(
                    (function() use ($e) {
                        $stream = GeneralUtility::makeInstance(BufferStream::class);
                        $stream->write($e->getMessage());
                        return $stream;
                    })()
                );
        }

        // check honey pot mechanism
        if (
            array_key_exists('honeypot', $definition)
            && ! (function(array $values, ServerRequestInterface $request) {
                    foreach($values as $name => $value) {
                        if (
                            ! array_key_exists($name, $request->getParsedBody())
                            || $request->getParsedBody()[$name] !== $value
                        ) {
                            return false;
                        }
                    }
                    return true;
                })($definition['honeypot']['values'], $request)
        ) {
            return $response
                ->withStatus(403)
                ->withBody(
                    (function() use ($definition) {
                        $stream = GeneralUtility::makeInstance(BufferStream::class);
                        $stream->write($definition['honeypot']['message']);
                        return $stream;
                    })()
                );
        }

        // process fields
        $processedFields = [];
        foreach($definition['fields'] as $name => $fieldDefinition) {
            $value = $request->getParsedBody()[$name] ?? null;
            if (array_key_exists('validators', $fieldDefinition)) {
                foreach($fieldDefinition['validators'] as $class => $options) {
                    if (!is_a($class, ValidatorInterface::class, true)) {
                        return $response
                            ->withStatus(500)
                            ->withBody(
                                (function() use ($request, $class, $name) {
                                    $stream = GeneralUtility::makeInstance(BufferStream::class);
                                    $stream->write(
                                        'Bad form definition "' . $request->getQueryParams()['form'] . '": Field validator "'
                                        . $class . '" of field "' . $name . '" does not implement ' . ValidatorInterface::class . ' interface'
                                    );
                                    return $stream;
                                })()
                            );
                    }
                    /** @var ValidatorInterface $validator */
                    $validator = GeneralUtility::makeInstance($class, $options);
                    $result = $validator->validate($value);
                    if ($result->hasErrors()) {
                        return $response
                            ->withStatus(422, 'Unprocessable Entity')
                            ->withBody(
                                (function() use ($name, $result) {
                                    $stream = GeneralUtility::makeInstance(BufferStream::class);
                                    $stream->write('Field "' . $name . '": ' . $result->getFirstError()->getMessage());
                                    return $stream;
                                })()
                            );
                    };
                }
            }
            $processedFields[$name] = $value;
        }

        // default to an empty answer. Finishers might intervene and deliver a more elaborate response
        $response = $response->withStatus(204);

        // process finishers
        foreach($definition['finishers'] as $class => $arguments) {
            if (!is_a($class, Finishing::class, true)) {
                return $response
                    ->withStatus(500)
                    ->withBody(
                        (function() use ($request, $class) {
                            $stream = GeneralUtility::makeInstance(BufferStream::class);
                            $stream->write(
                                'Bad form definition "' . $request->getQueryParams()['form'] . '": Form finisher "'
                                    . $class . '" does not implement ' . Finishing::class . ' interface'
                            );
                            return $stream;
                        })()
                    );
            }
            /** @var Finishing $finisher */
            $finisher = GeneralUtility::makeInstance($class);
            $response = $finisher->invoke(
                $request->getQueryParams()['form'],
                $request,
                $response,
                array_merge($arguments, ['fields' => $processedFields])
            );
        }

        return $response;
    }
}
