<?php

namespace VITD\Request\Tca;

/**
 * TCA Helper
 */
class Helper
{
    /**
     * Label requests
     *
     * (TYPO3 Backend has insane date formatting: d-m-y H:i)
     *
     * @param array $parameters Information about the record for which to get the title; contains keys 'table' (string)
     *     and 'row' (array)
     * @param mixed $parentObject Caller; unused
     *
     * @return void
     */
    public static function label(&$parameters, &$parentObject)
    {
        $parameters['title'] = sprintf(
            '[%s] %s: %s',
            $parameters['row']['type'],
            $parameters['row']['date'],
            $parameters['row']['title']
        );
    }
}
