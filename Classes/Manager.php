<?php

namespace VITD\Request;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Form Manager
 */
class Manager
{

    /**
     * Register a form definition
     *
     * @param string $id Form identification
     * @param string $definitionFile Path to form definition YAML file (full path or EXT:… syntax)
     *
     * @return void
     *
     * @throws \InvalidArgumentException 1516802726 If the form id is empty
     * @throws \RuntimeException 1516803178 If a form by the specified id is already defined
     * @throws \InvalidArgumentException 1516802231 If the form definition file does not exist, is outside the allowed
     *     pathes or is not accessible
     *
     * @api
     */
    public static function registerForm(string $id, string $definitionFile): void
    {
        if ('' === $id) {
            throw new \InvalidArgumentException('Form id must not be empty', 1516802726);
        }
        if (isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['request']['registeredForms'][$id])) {
            throw new \RuntimeException('Form "' . $id . '" already defined.', 1516803178);
        }

        $fullpath = GeneralUtility::getFileAbsFileName($definitionFile);
        if ($fullpath === '') {
            throw new \InvalidArgumentException('Cannot resolve form definition file "' . $definitionFile . '"', 1516802231);
        }

        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['request']['registeredForms'][$id] = $fullpath;
    }


    /**
     * Get a forms' definition
     *
     * @param string $id Form identification
     *
     * @return array Form definition (see documentation for details)
     *
     * @throws \RuntimeException 1516810320 If the requested form is not defined
     * @throws \RuntimeException 1516810459 If the form definition does not follow basic requirements
     * @throws ParseException If the form definition has syntax errors
     *
     * @internal
     */
    public static function getFormDefinition(string $id): array
    {
        if (!isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['request']['registeredForms'][$id])) {
            throw new \RuntimeException('Cannot resolve form', 1516810320);
        }
        $definition = Yaml::parse(
            file_get_contents($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['request']['registeredForms'][$id]),
            Yaml::PARSE_EXCEPTION_ON_INVALID_TYPE | Yaml::PARSE_DATETIME
        );
        if (!\is_array($definition)) {
            throw new \RuntimeException('Invalid form definition', 1516810459);
        }
        return array_merge(['fields' => [], 'finishers'=> []], $definition);
    }
}
