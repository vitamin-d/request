--
-- Request
--
create table tx_request_request (
	uid int not null auto_increment,
	pid int default 0 not null,

	type varchar(64),
	date datetime default current_timestamp,
	title varchar(255),
	content text,

	deleted tinyint unsigned default 0 not null,

	primary key (uid),
	key parent (pid)
);
